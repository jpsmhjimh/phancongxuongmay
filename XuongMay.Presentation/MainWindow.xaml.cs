﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using OfficeOpenXml;
using OfficeOpenXml.Table;
using System.Data;
using System.Data.OleDb;
using XuongMay.Core;
using System.IO;
namespace XuongMay.Presentation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        clsXuLi xuongmay = new clsXuLi();
        public MainWindow()
        {
            
            InitializeComponent();
        }

        // đường dẫn file import
        string fileName;

        string savefile;

        //Export dữ liệu ra
        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "PhanCong";
            dlg.Filter = "Excel|*.xlsx";
            Nullable<bool> a = dlg.ShowDialog();
           
            if (a == true)
            {

                if (File.Exists(dlg.FileName))
                    File.Delete(dlg.FileName);
                //Tajo file
                var newFile = new FileInfo(dlg.FileName);
                //Tao ra 1 package
                using (var Excel = new ExcelPackage(newFile))
                {

                    //Add worksheet
                    ExcelWorksheet worksheet = Excel.Workbook.Worksheets.Add("KQ"); //có thể thay kq bằng tên bảng tùy ý
                    //Tao 1 dataview de nhan du lie
                    DataView dv = new DataView();
                    dv = (DataView)dataGrid1.DataContext;
                    dataGrid1.DataContext = dv;
                    //Loading table
                    worksheet.Cells["A1"].LoadFromDataTable(dv.Table, true, TableStyles.None);
                    Excel.Save();
                }
                MessageBox.Show(" Đã lưu file thành công!");

            }
        }

        //Import dữ liệu vào
        string fileN = "1";
        int loai = 1;
        private void btnImport_Click(object sender, RoutedEventArgs e)
        {
            btnsua.Visibility = Visibility.Hidden;
            Import test = new Import();
            test.ShowDialog();
            fileN = test.fileName;
            loai = test.loai;
            if (loai == 4)
            {
                fileN = "1";
                return;
            }
            if (loai == 10)
            {
                fileN = "1";
            }
            if (test.fileName == "")
            {
                fileN = "1";
                MessageBox.Show("Import thất bại");
                return;
            }
            else
                MessageBox.Show("Import thành công");

            phancong = true;
        }
        private bool phancong = true;
        //xuất kết quả phân công
        DataTable dt = new DataTable();
        private void btnKetQua_Click(object sender, RoutedEventArgs e)
        {
            btnsua.Visibility = Visibility.Hidden;
            if (phancong)
            {
                phancong = false;
                xuongmay.OdbConnect(fileN);
                dt = xuongmay.KetQuaPhanCong(fileN);
             
            }
           
            dataGrid1.DataContext = dt.DefaultView;

        }
        //update thông tin
        public void UpdateTo()
        {
            MessageBoxResult dt = MessageBox.Show("Bạn Có Muốn Sửa Dữ Liệu", "Thông Báo!", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (dt == MessageBoxResult.Yes)
            {
                DataView dv = new DataView();
                dv = (DataView)dataGrid1.DataContext;
                xuongmay.tomayupdate(dv.Table);
                MessageBox.Show("Đã cập nhật dữ liệu thành công!");
            }
            else
            {

            }

        }
        public void UpdateSanPham()
        {
            MessageBoxResult dt = MessageBox.Show("Bạn Có Muốn Sửa Dữ Liệu", "Thông Báo!", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (dt == MessageBoxResult.Yes)
            {
                DataView dv = new DataView();
                dv = (DataView)dataGrid1.DataContext;
                xuongmay.sanphamupdate(dv.Table);
                MessageBox.Show("Đã cập nhật dữ liệu thành công!");
            }
            else
            {

            }
        }
        public void UpdateNS()
        {
            MessageBoxResult dt = MessageBox.Show("Bạn Có Muốn Sửa Dữ Liệu", "Thông Báo!", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (dt == MessageBoxResult.Yes)
            {
                DataView dv = new DataView();
                dv = (DataView)dataGrid1.DataContext;
                xuongmay.Autoupdate(dv.Table);
                MessageBox.Show("Đã cập nhật dữ liệu thành công!");
            }
            else
            {

            }
        }
        int type = 0;
        private void menu_dsto_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                type = 2;
                btnExport.Visibility = Visibility.Visible;
                btnsua.Visibility = Visibility.Visible;
                xuongmay.OdbConnect(fileN);
                dataGrid1.DataContext = xuongmay.ToMay().DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void menu_dssanpham_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                type = 3;
                btnExport.Visibility = Visibility.Visible;
                btnsua.Visibility = Visibility.Visible;
                xuongmay.OdbConnect(fileN);
                dataGrid1.DataContext = xuongmay.dssanpham().DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void menu_ns_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                type = 1;
                btnExport.Visibility = Visibility.Visible;
                btnsua.Visibility = Visibility.Visible;
                xuongmay.OdbConnect(fileN);
                dataGrid1.DataContext = xuongmay.NangSuatTM().DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnabout_Click(object sender, RoutedEventArgs e)
        {
            btnsua.Visibility = Visibility.Hidden;
            about ab = new about();
            ab.ShowDialog();

        }

        private void btnsua_Click(object sender, RoutedEventArgs e)
        {
            try{
                btnExport.Visibility = Visibility.Visible;
                switch (type)
                {
                    case 1: UpdateNS(); break;
                    case 2: UpdateTo(); break;
                    case 3: UpdateSanPham(); break;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void mn_DsDH_Click(object sender, RoutedEventArgs e)
        {
            try{
            btnsua.Visibility = Visibility.Hidden;
            dataGrid1.DataContext = xuongmay.DonHang("1").DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mn_DH_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnsua.Visibility = Visibility.Hidden;
                if (fileN == "1") MessageBox.Show("Không có đơn hàng được import");
                else
                    dataGrid1.DataContext = xuongmay.DonHang(fileN).DefaultView;
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void mn_DsPc_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnsua.Visibility = Visibility.Hidden;
                if (phancong)
                {
                    phancong = false;
                    xuongmay.OdbConnect(fileN);
                    dt = xuongmay.KetQuaPhanCong(fileN);
                }
                dataGrid1.DataContext = dt.DefaultView;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void mn_PC_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                btnsua.Visibility = Visibility.Hidden;
                if (fileN != "1")
                {
                    if (phancong)
                    {
                        phancong = false;
                        xuongmay.OdbConnect(fileN);
                        dt = xuongmay.KetQuanImport(fileN);

                    }
                    dataGrid1.DataContext = dt.DefaultView;
                }
                else
                {
                    MessageBox.Show("Không có đơn hàng nào import");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                btnsua.Visibility = Visibility.Hidden;
                if (phancong)
                {
                    phancong = false;
                    xuongmay.OdbConnect(fileN);
                    dt = xuongmay.KetQuaPhanCong(fileN);

                }
                dataGrid1.DataContext = dt.DefaultView;
            }catch(Exception ex){
                MessageBox.Show(ex.Message);
            }
        }
    }
}