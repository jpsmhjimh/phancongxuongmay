﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.OleDb;

namespace XuongMay.Core
{
    public partial class clsXuLi 
    {
        /// <summary>
        /// khai báo biến kết nối sử dụng
        /// </summary>
        OleDbConnection Con;
        OleDbDataAdapter da;
        OleDbCommand cmd;
        DataSet ds;
        /// <summary>
        /// 
        /// khai báo về các danh sách đơn hàng, tổ may, đơn hàng nhận được
        /// </summary>
        clsDanhSachDonHang dsDonHang;
        clsDanhSachTo dsToMay;

        clsDanhSachDonHang dsDonHangLoai;
        string _strCon = "1";
        //Kết nối csdl
        public OleDbConnection OdbConnect(string connect)
        {
            _strCon = connect;
            string strcon;
            //Chuỗi Kết Nối Mặc Định là access
            if(connect == "1")
                strcon = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source=..\..\data\XuongMay.mdb";
            //chuỗi kết nối excel
            else strcon = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source="+connect+";Extended Properties=" + '"' + "Excel 8.0;HDR=YES" + '"';
            Con = new OleDbConnection(strcon);
            return Con;
        }
        //Bảng các đơn hàng được import vào hệ thống
        public DataTable DonHang(string connect)
        {
            dsDonHang = new clsDanhSachDonHang();
            DataTable temp = new DataTable();
            // Kiểm tra xem chuỗi đưa vào là kết nối mặc định hay k?
            if (connect == "1")
            {
                da = new OleDbDataAdapter("Select * from DonHang ", OdbConnect("1"));
                da.Fill(temp);
                // trả ra đơn hàng trước đó
                return temp;
            }
            //lấy dữ liệu đơn hàngd
            da = new OleDbDataAdapter("Select * from [DonHang$]", OdbConnect(connect));
            da.Fill(temp);

            //Kiếm tra sự tồn tại của bảng dữ liệu
            if (temp.Rows.Count == 0) return temp;
            // duyệt bảng đơn hàng để thêm vào ds đơn hàng
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                try
                {
                    clsDonHang dh = new clsDonHang();
                    dh.MaDonHang = temp.Rows[i][1].ToString();
                    dh.SanPham = new clsSanPham();
                    dh.SanPham.MaSanPham = temp.Rows[i][2].ToString();
                    dh.SanPham.StylePO = temp.Rows[i][8].ToString();
                    dh.SoLuongDat = int.Parse(temp.Rows[i][3].ToString());
                    dh.SoLuongMay = int.Parse(temp.Rows[i][4].ToString());
                    dh.NgayBD = DateTime.Parse(temp.Rows[i][5].ToString());
                    dh.NgayKT = DateTime.Parse(temp.Rows[i][6].ToString());
                    dh.Po = temp.Rows[i][7].ToString();
                    dh.NgayNPL = DateTime.Now;
                    //Thêm 1 đơn hàng vào danh sách
                    dsDonHang.Add(dh);
                }
                catch (Exception ex) { }
                
            }
            //Tra ra kết quả các tổ may
            return temp;
        }

        // Trả về kết quả bảng các tổ may gồm danh sách tổ, năng suất và thời gian truyền
        public DataTable ToMay()
        {
            DataTable temp = new DataTable();
            

            // lấy dữ liệu về tổ may mặc định ở access lên
            da = new OleDbDataAdapter("SELECT * FROM TOMAY", OdbConnect("1"));

            da.Fill(temp);

            // Kiểm tra sự tồn tại của bảng tổ may

            if (temp.Rows.Count == 0) return temp;
            dsToMay = new clsDanhSachTo();

            //duyệt bảng năng suất để đổ dữ liệu năng suất vào tổ may
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                DataTable ns = new DataTable();
                clsToMay tm = new clsToMay();
                tm.TenTo = temp.Rows[i][1].ToString();
                //Năng suất của tổ may

                // lấy dữ liệu về tổ may mặc định ở access lên
                da = new OleDbDataAdapter(string.Format("SELECT * FROM NangSuat Where TenTo='{0}'",tm.TenTo), OdbConnect("1"));
                
                da.Fill(ns);
                tm.NangSuat = new clsDanhSachSanPham();
                for (int j = 0; j < ns.Rows.Count; j++)
                {
                    clsSanPham sp = new clsSanPham();
                    sp.MaSanPham = ns.Rows[j][1].ToString();
                    sp.SoLuong = float.Parse(ns.Rows[j][3].ToString());
                    tm.NangSuat.Add(sp);
                }
                if (temp.Rows[i][2].ToString() != "")
                    tm.SanSang = DateTime.Parse(temp.Rows[i][2].ToString());
                else
                {
                    tm.SanSang = DateTime.Now;
                }
                dsToMay.Add(tm);
            }
            //
            return temp;
        }

        public DataTable KetQuanImport(string connect)
        {
            DonHang(connect);
            DataTable dtdh = new DataTable();
            for (int i = 0; i < DonHang(connect).Rows.Count; i++)
            {
                da = new OleDbDataAdapter(string.Format("select * from donhangnhan where MaDonHang='{0}' order by Chuyenmay", DonHang(connect).Rows[i][1].ToString()), OdbConnect("1"));
                da.Fill(dtdh);
                dtdh.Rows.Add(NewRows(dtdh));
            }
            return dtdh;
        }
        public DataTable KetQuaPhanCong(string connect)
        {
            //Check();
            ToMay();
            if (connect == "1") return XuLiPC();
            DonHang(connect);
            //duyệt các đơn hàng trong ds đơn hàng để xem khả năng nhận đơn hàng
            clsDanhSachDonHang dsDonHangLoait = new clsDanhSachDonHang();
            dsDonHangLoait= DHLoai(dsDonHang,connect);
            clsDonHang dd = new clsDonHang();
            dd = dsDonHangLoait.Head;
            if (dd != null)
            {
                TangCa();
                dsDonHangLoai = new clsDanhSachDonHang();
                dsDonHangLoai = DHLoai(dsDonHangLoait, connect);
            }
            //Duyệt đơn hàng bị loại sau tăng ca
            
            clsDonHang loai = new clsDonHang();
            if (dsDonHangLoai != null)
            {
                loai = dsDonHangLoai.Head;
                while (loai != null)
                {
                    try
                    {
                        DataTable dtq = new DataTable();
                        da = new OleDbDataAdapter("select id from donhang order by id", OdbConnect("1"));
                        da.Fill(dtq); int cc;
                        if (dtq.Rows.Count != 0)
                            cc = int.Parse(dtq.Rows[dtq.Rows.Count - 1][0].ToString());
                        else
                            cc = 1;
                        cc++;
                        string SQL = string.Format("Insert Into DonHang ");// (PO,KHACHHANG,MAHANG,STYLE,TENHANG,NGAYHD,NGAYNPL,SOLUONG,SOLUONGMAY,CHUYENMAY,NGAYCAT,NGAYVAOCHUYEN,NGAYRACHUYEN,NANGSUAT,SONGAY,SONGAYHT,NGAYMAYXONG,NGAYCUOI,NGAYXUAT)");
                        SQL += string.Format(" Values({0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')"
                            , cc.ToString(), loai.MaDonHang, loai.SanPham.MaSanPham,
                            loai.SoLuongDat, loai.SoLuongMay, loai.NgayBD.ToShortDateString(),
                            loai.NgayKT.ToShortDateString(), loai.Po, loai.SanPham.StylePO,
                            "Loại",
                            DateTime.Now.ToShortDateString()
                            );
                        cmd = new OleDbCommand(SQL, OdbConnect("1"));
                        Con.Open();
                        cmd.ExecuteNonQuery();
                        Con.Close();
                    }

                    catch (Exception ex) { }
                    loai = loai.Next;
                }
            }
            return XuLiPC();
        }
        private DataTable XuLiPC()
        {
            DataTable dtp = new DataTable();
            DataTable dtm = new DataTable();

            DataSet ds = new DataSet();
            da = new OleDbDataAdapter("select distinct ChuyenMay from donhangnhan order by Chuyenmay", OdbConnect("1"));
            da.Fill(dtm);
            
            for (int i = 0; i < dtm.Rows.Count; i++)
            {
                da = new OleDbDataAdapter(string.Format("select * from donhangnhan where chuyenmay='{0}' order by Chuyenmay",dtm.Rows[i][0].ToString()), OdbConnect("1"));
                da.Fill(dtp);
                dtp.Rows.Add(NewRows(dtp));
            }
            return dtp;
        }
        private DataRow NewRows(DataTable dtp)
        {
            return dtp.NewRow();
        }
        private int SoTo(clsDanhSachTo ds)
        {
            clsToMay tm = new clsToMay();
            tm = ds.Head;
            int i = 0;
            while (tm != null)
            {
                i++;
                tm = tm.Next;
            }
            return i;
        }

        //Xếp các tổ vào đơn hàng có thể nhận
        private clsNodeDH XepLich(clsDanhSachTo ToDuyet, clsDonHang dhFirst, string connect)
        {
            clsToMay tmDuyet = new clsToMay();
            tmDuyet = ToDuyet.Head;
            float _LuongCon = dhFirst.SoLuongMay;
            clsNodeDH DH = new clsNodeDH();
            DH.ToLam = new clsToLam[SoTo(ToDuyet)];
            int tolam = 0;
            while (tmDuyet != null)
            {
                clsSanPham sp = new clsSanPham();
                sp = tmDuyet.NangSuat.Head;
                while (sp != null)
                {
                    if (sp.MaSanPham == dhFirst.SanPham.MaSanPham) break;
                    sp = sp.Next;
                }

                DH.ToLam[tolam] = new clsToLam();
                DH.ToLam[tolam].NgayBD = dhFirst.NgayBD;
                DH.ToLam[tolam].ChuyenMay = tmDuyet;

                DH.ToLam[tolam].NgayCat = DateTime.Now;
                DH.ToLam[tolam].NgayVaoChuyen = DH.ToLam[tolam].NgayCat.AddDays(1);
                if (tmDuyet.SanSang < DateTime.Now.AddDays(2))
                    DH.ToLam[tolam].NgayRaChuyen = DateTime.Now.AddDays(2);
                else
                    DH.ToLam[tolam].NgayRaChuyen = tmDuyet.SanSang;
                DH.ToLam[tolam].NangSuatDK = (int)sp.SoLuong;

                DH.ToLam[tolam].SoNgayDuKien = tmDuyet.Thoigian;
                DH.ToLam[tolam].SoNgayDuPhong = tmDuyet.ThoiGianXong + tmDuyet.Thoigian;
                DH.ToLam[tolam].NgayMayXong = DH.ToLam[tolam].NgayRaChuyen.AddDays(DH.ToLam[tolam].SoNgayDuPhong);
                if (DH.ToLam[tolam].NgayMayXong >= dhFirst.NgayKT.AddDays(-1))
                {
                    DH.ToLam[tolam].NgayCuoi = dhFirst.NgayKT;
                }
                else
                {
                    DH.ToLam[tolam].NgayCuoi = DH.ToLam[tolam].NgayMayXong.AddDays(+2);
                }
                DH.ToLam[tolam].NgayXuatDi = dhFirst.NgayKT;
                
                tmDuyet.SanSang = DH.ToLam[tolam].NgayMayXong.AddDays(1);
                if (tmDuyet.SanSang.DayOfWeek.ToString() == "Sunday")
                    tmDuyet.SanSang = tmDuyet.SanSang.AddDays(1);
                //Cập nhật lại thời gian sẵn sàng của   tổ may
                clsToMay a = new clsToMay();
                a = dsToMay.Head;
                while (a != null)
                {
                    if (a.TenTo == DH.ToLam[tolam].ChuyenMay.TenTo)
                    {
                        a.SanSang = DH.ToLam[tolam].ChuyenMay.SanSang;

                        cmd = new OleDbCommand(string.Format("Update ToMay set SanSang= '{0}' where TenTo = '{1}'", DH.ToLam[tolam].ChuyenMay.SanSang.ToShortDateString(), a.TenTo), OdbConnect("1"));
                       
                        Con.Open();
                        cmd.ExecuteNonQuery();
                        Con.Close();
                    }
                    a = a.Next;
                }
                tolam++;
                tmDuyet = tmDuyet.Next;
            }
            return DH;
        }
        //Xử lý tăng cả
        private void TangCa()
        {
            //duyệt bảng chứa danh sách tổ may
            DataTable tblTM = new DataTable();
            da = new OleDbDataAdapter(string.Format("select * from ToMay order by id"), OdbConnect("1"));
            da.Fill(tblTM);
            
            //duyệt từng đơn hàng mà tổ may thứ i thực hiện
            
            
            for (int i = 0; i < tblTM.Rows.Count; i++)
            {
                //duyệt bảng chứa danh sách đơn hàng được nhận của tổ may thứ i trên bảng tổ may
                DataTable tblDH = new DataTable();
                da = new OleDbDataAdapter(string.Format("select * from donhangnhan where NgayXuatDi > {0} and ChuyenMay = '{1}' order by id", DateTime.Now.ToShortDateString(),tblTM.Rows[i][1].ToString()), OdbConnect("1"));
                da.Fill(tblDH);

                //tạo biến thời gian dịch chuyển thời gian khi tăng ca
                int timeskip = 0;
                //duyệt từng đơn hàng trong bảng đơn hàng nhận
                for (int ji = 0; ji < tblDH.Rows.Count; ji++)
                {
                    //Cập nhật thời gian ra chuyển theo timeskip
                    tblDH.Rows[ji][10] = DateTime.Parse(tblDH.Rows[ji][10].ToString()).AddDays(-timeskip).ToShortDateString();

                    if (DateTime.Parse(tblDH.Rows[ji][10].ToString()) > DateTime.Now)
                    {
                        for (int q = 1; q <= int.Parse(tblDH.Rows[ji][12].ToString()); q++)
                        {
                            if (q * 1.15 * int.Parse(tblDH.Rows[ji][11].ToString()) >= int.Parse(tblDH.Rows[ji][6].ToString()))
                            {
                                //cập nhật thời gian dịch chuyển thời gian khi tăng ca
                                timeskip += int.Parse(tblDH.Rows[ji][12].ToString()) - q;
                                tblDH.Rows[ji][12] =  q.ToString();
                                tblDH.Rows[ji][13] = (q + ChuNhat(DateTime.Parse(tblDH.Rows[ji][10].ToString()),DateTime.Parse(tblDH.Rows[ji][10].ToString()).AddDays(q))).ToString();
                                tblDH.Rows[ji][17] = tblDH.Rows[ji][10].ToString();
                                tblDH.Rows[ji][14] = DateTime.Parse(tblDH.Rows[ji][10].ToString()).AddDays(int.Parse(tblDH.Rows[ji][13].ToString()));
                                break;
                            }
                        }
                    }
                    else
                    {
                        // thời gian đã làm
                        int day = int.Parse((DateTime.Now - DateTime.Parse(tblDH.Rows[ji][10].ToString())).ToString());

                        // lượng hàng đã làm
                        int hanglam = int.Parse(tblDH.Rows[ji][11].ToString()) * day;

                        for (int q = 1; q <= int.Parse(tblDH.Rows[ji][12].ToString()); q++)
                        {
                            if (q * 1.15 * int.Parse(tblDH.Rows[ji][11].ToString()) >= int.Parse(tblDH.Rows[ji][6].ToString()) - hanglam)
                            {
                                //cập nhật thời gian dịch chuyển thời gian khi tăng ca
                                timeskip += int.Parse(tblDH.Rows[ji][12].ToString()) - q;
                                tblDH.Rows[ji][12] = (q+ day).ToString();
                                tblDH.Rows[ji][13] = (q + day + ChuNhat(DateTime.Parse(tblDH.Rows[ji][10].ToString()), DateTime.Parse(tblDH.Rows[ji][10].ToString()).AddDays(q))).ToString();
                                tblDH.Rows[ji][14] = DateTime.Parse(tblDH.Rows[ji][10].ToString()).AddDays(int.Parse(tblDH.Rows[ji][13].ToString()));
                                tblDH.Rows[ji][17] = DateTime.Now.ToShortDateString();
                                break;
                            }
                        }
                    }
                    //Cập nhật lại thời gian ở đơn hàng củaw tôt may thứ i
                    for (int mk = 0; mk < tblDH.Rows.Count; mk++)
                    {
                        try
                        {
                            string SQL = string.Format("Update DonHangNhan set ");
                            SQL += string.Format("Ngayrachuyen = '{0}',SoNgay = '{1}', SoNgayHT= '{2}', TangCa = '{3}' Where ID ={4}",
                                DateTime.Parse(tblDH.Rows[mk][10].ToString()).ToShortDateString(), tblDH.Rows[mk][12].ToString(), tblDH.Rows[mk][13].ToString(), DateTime.Parse(tblDH.Rows[mk][17].ToString()).ToShortDateString(), tblDH.Rows[mk][0].ToString()
                                );
                            cmd = new OleDbCommand(SQL, OdbConnect("1"));
                            Con.Open();
                            cmd.ExecuteNonQuery();
                            Con.Close();
                        }
                        catch (Exception ex) { }
                    }
                }
                //Cập nhật lại thời gian sẵn sàng  ở tổ may thứ i
                if (tblDH.Rows.Count != 0)
                {
                    string SQL1 = string.Format("Update ToMay set ");
                    SQL1 += string.Format("SanSang = '{0}' where TenTo = '{1}'",
                        DateTime.Parse(tblDH.Rows[tblDH.Rows.Count - 1][14].ToString()).AddDays(1).ToShortDateString(), tblTM.Rows[i][1].ToString()
                        );
                    cmd = new OleDbCommand(SQL1, OdbConnect("1"));
                    Con.Open();
                    cmd.ExecuteNonQuery();
                    Con.Close();
                }
            }
        }
        //Tính số ngày chủ nhật trong 1 khoảng thời gian nào đó
        private int ChuNhat(DateTime F, DateTime S)
        {
            int cn = 0;
            int so = int.Parse((S - F).Days.ToString());
            for (int i = 0; i <= so; i++)
            {
                if (F.AddDays(i).DayOfWeek.ToString() == "Sunday")
                    cn++;
            }
            return cn;
        }

        //add data
        private clsDanhSachDonHang DHLoai(clsDanhSachDonHang listDonHang, string connect)
        {
            clsDanhSachDonHangNhan dsDonHangNhan = new clsDanhSachDonHangNhan();
            clsDanhSachDonHang dsDonHangBiLoai = new clsDanhSachDonHang();
            clsDonHang dhFirst = new clsDonHang();
            dhFirst = listDonHang.Head;
            while (dhFirst != null)
            {
                //ds to ranh va to ban
                clsDanhSachTo ToRanhRoi = new clsDanhSachTo();
                clsDanhSachTo ToBan = new clsDanhSachTo();

                DateTime DateBD = dhFirst.NgayBD;
                DateTime DateKT = dhFirst.NgayKT;

                clsToMay tmFirst = new clsToMay();
                tmFirst = dsToMay.Head;

                // duyệt danh sách tổ rảnh rỗi có thể đảm nhận đơn hàng
                while (tmFirst != null)
                {

                    ToRanhRoi.Add(tmFirst);
                    tmFirst = tmFirst.Next;
                }
                clsDanhSachTo ToDuyet = new clsDanhSachTo();

                tmFirst = ToRanhRoi.Head;
                float _soLuongHang = 0;

                while (tmFirst != null)
                {
                    if (tmFirst.SanSang < DateTime.Now)
                        tmFirst.SanSang = DateTime.Now;
                    int iThoiGian = int.Parse((dhFirst.NgayKT - tmFirst.SanSang).Days.ToString()) - 1;
                    if (iThoiGian <= 0)
                    {
                        tmFirst = tmFirst.Next;
                        continue;
                    }
                    clsSanPham sp = new clsSanPham();
                    sp = tmFirst.NangSuat.Head;
                    while (sp != null)
                    {
                        if (sp.MaSanPham == dhFirst.SanPham.MaSanPham) break;

                        sp = sp.Next;
                    }
                    if (sp == null)
                    {
                        tmFirst = tmFirst.Next;
                        continue;
                    }
                    _soLuongHang += iThoiGian * sp.SoLuong;
                    tmFirst.Thoigian = iThoiGian;
                    tmFirst.ThoiGianXong = ChuNhat(tmFirst.SanSang, tmFirst.SanSang.AddDays(iThoiGian));
                    ToDuyet.Add(tmFirst);

                    if (_soLuongHang >= dhFirst.SoLuongMay)
                    {
                        float _LuongCon = dhFirst.SoLuongMay;
                        clsToMay xet = new clsToMay();
                        xet = ToDuyet.Head;
                        while (xet != null)
                        {
                            if (_LuongCon <= sp.SoLuong * xet.Thoigian)
                            {
                                for (int i = 1; i <= xet.Thoigian; i++)
                                {
                                    if (_LuongCon <= sp.SoLuong * i)
                                    {
                                        xet.Thoigian = i;
                                        xet.ThoiGianXong = ChuNhat(xet.SanSang, xet.SanSang.AddDays(i));
                                        break;
                                    }
                                }
                            }
                            else
                                _LuongCon = _LuongCon - sp.SoLuong * xet.Thoigian;
                            xet = xet.Next;
                        }
                        break;
                    }
                    tmFirst = tmFirst.Next;
                }

                if (tmFirst == null)
                {
                    ///don hang bi loai;
                    dsDonHangBiLoai.Add(dhFirst);
                    dhFirst = dhFirst.Next;
                    continue;
                }
                clsNodeDH DH = new clsNodeDH();
                //Duyet danh sach to lam don hang dhFirst
                DH = XepLich(ToDuyet, dhFirst, connect);
                DH.Donhang = new clsDonHang();
                DH.Donhang = dhFirst;
                dsDonHangNhan.Add(DH);
                dhFirst = dhFirst.Next;
            }

            //Tao bang hien thi ket qua don hang nhan
            clsNodeDH dhnhFirst = dsDonHangNhan.Head;
            DataTable dt = new DataTable();
            while (dhnhFirst != null)
            {
                for (int o = 0; o < dhnhFirst.ToLam.Length; o++)
                {
                    try
                    {
                        da = new OleDbDataAdapter("select id from donhangnhan order by id", OdbConnect("1"));
                        da.Fill(dt); int cc;
                        if (dt.Rows.Count != 0)
                            cc = int.Parse(dt.Rows[dt.Rows.Count - 1][0].ToString());
                        else
                            cc = 1;
                        cc++;
                        string SQL = string.Format("Insert Into DonHangNhan ");// (PO,KHACHHANG,MAHANG,STYLE,TENHANG,NGAYHD,NGAYNPL,SOLUONG,SOLUONGMAY,CHUYENMAY,NGAYCAT,NGAYVAOCHUYEN,NGAYRACHUYEN,NANGSUAT,SONGAY,SONGAYHT,NGAYMAYXONG,NGAYCUOI,NGAYXUAT)");
                        SQL += string.Format(" Values({0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','Không')"
                            , cc.ToString(), dhnhFirst.Donhang.MaDonHang, dhnhFirst.Donhang.SanPham.MaSanPham,
                            dhnhFirst.Donhang.SanPham.StylePO, dhnhFirst.Donhang.NgayBD.ToShortDateString(),
                            dhnhFirst.Donhang.NgayNPL.ToShortDateString(), dhnhFirst.Donhang.SoLuongMay.ToString(),
                            dhnhFirst.ToLam[o].ChuyenMay.TenTo, dhnhFirst.ToLam[o].NgayCat.ToShortDateString(), dhnhFirst.ToLam[o].NgayVaoChuyen.ToShortDateString(),
                            dhnhFirst.ToLam[o].NgayRaChuyen.ToShortDateString(), dhnhFirst.ToLam[o].NangSuatDK.ToString(), dhnhFirst.ToLam[o].SoNgayDuKien.ToString(),
                            dhnhFirst.ToLam[o].SoNgayDuPhong.ToString(), dhnhFirst.ToLam[o].NgayMayXong.ToShortDateString(), dhnhFirst.ToLam[o].NgayCuoi.ToShortDateString(),
                            dhnhFirst.ToLam[o].NgayXuatDi.ToShortDateString()
                            );
                        cmd = new OleDbCommand(SQL, OdbConnect("1"));
                        Con.Open();
                        cmd.ExecuteNonQuery();
                        Con.Close();
                    }
                    catch (Exception ex) { }
                }
                //Thêm đơn hàng này vào danh sách đơn hàng nhận

                try
                {
                    da = new OleDbDataAdapter("select id from donhang order by id", OdbConnect("1"));
                    da.Fill(dt); int cc;
                    if (dt.Rows.Count != 0)
                        cc = int.Parse(dt.Rows[dt.Rows.Count - 1][0].ToString());
                    else
                        cc = 1;
                    cc++;
                    string SQL = string.Format("Insert Into DonHang ");// (PO,KHACHHANG,MAHANG,STYLE,TENHANG,NGAYHD,NGAYNPL,SOLUONG,SOLUONGMAY,CHUYENMAY,NGAYCAT,NGAYVAOCHUYEN,NGAYRACHUYEN,NANGSUAT,SONGAY,SONGAYHT,NGAYMAYXONG,NGAYCUOI,NGAYXUAT)");
                    SQL += string.Format(" Values({0},'{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}')"
                        , cc.ToString(), dhnhFirst.Donhang.MaDonHang, dhnhFirst.Donhang.SanPham.MaSanPham,
                        dhnhFirst.Donhang.SoLuongDat, dhnhFirst.Donhang.SoLuongMay, dhnhFirst.Donhang.NgayBD.ToShortDateString(),
                        dhnhFirst.Donhang.NgayKT.ToShortDateString(), dhnhFirst.Donhang.Po, dhnhFirst.Donhang.SanPham.StylePO,
                        "Nhận",
                        dhnhFirst.ToLam[0].NgayCat.ToShortDateString()
                        );
                    cmd = new OleDbCommand(SQL, OdbConnect("1"));
                    Con.Open();
                    cmd.ExecuteNonQuery();
                    Con.Close();
                }
                catch (Exception ex) { }


                dhnhFirst = dhnhFirst.Next;
            }
            return dsDonHangBiLoai;
        }

        // lấy dữ liệu về tổ may
        public DataTable NangSuatTM()
        {
            DataTable temp = new DataTable();

            // lấy dữ liệu về tổ may mặc định ở access lên
            da = new OleDbDataAdapter("SELECT * FROM NANGSUAT order by ID", OdbConnect("1"));

            da.Fill(temp);
            return temp;
        }
        //lay thông tin bảng sản phẩm
        public DataTable dssanpham()
        {
            DataTable temp = new DataTable();
            // lấy dữ liệu về tổ may mặc định ở access lên
            da = new OleDbDataAdapter("SELECT * FROM SanPham", OdbConnect("1"));
            da.Fill(temp);
            return temp;
        }

        //thêm, sửa , xóa trực tiếp trên datagrip vào bảng năng suất
        public void Autoupdate(DataTable dt)
        {
            da = new OleDbDataAdapter("SELECT * FROM NangSuat", OdbConnect("1"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i][0] = (i + 1).ToString();
            }
            OleDbCommandBuilder cmd = new OleDbCommandBuilder(da);
            da.Update(dt);
        }
        //thêm,sửa,xóa bảng tổ máy
        public void tomayupdate(DataTable dt)
        {
            da = new OleDbDataAdapter("SELECT * FROM ToMay", OdbConnect("1"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i][0] = (i + 1).ToString();
            }
            OleDbCommandBuilder cmd = new OleDbCommandBuilder(da);
            da.Update(dt);
        }
        //thêm,sửa,xóa bảng sản phẩm
        public void sanphamupdate(DataTable dt)
        {
            da = new OleDbDataAdapter("SELECT * FROM SanPham", OdbConnect("1"));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dt.Rows[i][0] = (i + 1).ToString();
            }
            OleDbCommandBuilder cmd = new OleDbCommandBuilder(da);
            da.Update(dt);
        }

    }
}
