﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XuongMay.Core
{
   public class clsToMay
    {
        // Khai báo biến
        string _tenTo;
        DateTime _sanSang = new DateTime();

        public DateTime SanSang
        {
            get { return _sanSang; }
            set { _sanSang = value; }
        }

        public bool TrangThai()
        {
            if (_sanSang <= DateTime.Parse(DateTime.Now.ToShortDateString()))
                return true;
            else
                return false;
        }

        public string TenTo
        {
            get { return _tenTo; }
            set { _tenTo = value; }
        }
        clsDanhSachSanPham _nangSuat;

        public clsDanhSachSanPham NangSuat
        {
            get { return _nangSuat; }
            set { _nangSuat = value; }
        }
        clsToMay _next;

        public clsToMay Next
        {
            get { return _next; }
            set { _next = value; }
        }
       //Thời gian xong theo năng suất
        int _thoigian;

        public int Thoigian
        {
            get { return _thoigian; }
            set { _thoigian = value; }
        }
       //Thời gian xong khi tính cả những ngày nghỉ
        int _thoiGianXong;

        public int ThoiGianXong
        {
            get { return _thoiGianXong; }
            set { _thoiGianXong = value; }
        }

        public clsToMay()
        {
            Next = null;
            TenTo = "";
            SanSang = DateTime.Parse(DateTime.Now.ToShortDateString());
        }
    }
    //Tạo 1 node về sản phẩm để dùng cho năng xuất và số lượng sản phẩm trong đơn đặt hàng
    public class clsSanPham
    {
        string _maSanPham;

        public string MaSanPham
        {
            get { return _maSanPham; }
            set { _maSanPham = value; }
        }

        string _tenSanPham;

        public string TenSanPham
        {
            get { return _tenSanPham; }
            set { _tenSanPham = value; }
        }

        string _stylePO="";

        public string StylePO
        {
            get { return _stylePO; }
            set { _stylePO = value; }
        }
        float _soLuong;

        public float SoLuong
        {
            get { return _soLuong; }
            set { _soLuong = value; }
        }

        public clsSanPham()
        {
            MaSanPham = "";
            SoLuong = 0;
        }
        public clsSanPham Next = null;
    }
    public class clsDanhSachSanPham
    {
        clsSanPham _head, _tail;

        public clsSanPham Tail
        {
            get { return _tail; }
            set { _tail = value; }
        }

        public clsSanPham Head
        {
            get { return _head; }
            set { _head = value; }
        }

        public clsDanhSachSanPham()
        {
            Head = null;
            Tail = null;
        }

        public void Add(clsSanPham DH)
        {
            clsSanPham Temp = new clsSanPham();
            Temp.MaSanPham = DH.MaSanPham;
            Temp.SoLuong = DH.SoLuong;
            Temp.StylePO = DH.StylePO;
            Temp.TenSanPham = DH.TenSanPham;
            if (Head == null)
            {
                Head = Tail = Temp;
            }
            else
            {
                Tail.Next = Temp;
                Tail = Temp;
            }
        }

        public void Delete(clsSanPham DH)
        {
            clsSanPham Temp = new clsSanPham();
            Temp.MaSanPham = DH.MaSanPham;
            Temp.SoLuong = DH.SoLuong;
            Temp.StylePO = DH.StylePO;
            Temp.TenSanPham = DH.TenSanPham;
            clsSanPham q = new clsSanPham();

            Temp = Head;
            if (Temp == null)
            {
                return;
            }
            else
            {
                q = Temp; Temp = Temp.Next;

                if (Temp != null)
                {
                    if (Temp == Tail)
                    {
                        Tail = q;
                        Tail.Next = null;
                    }
                    else
                    {
                        q.Next = Temp.Next;
                        Temp = null;
                    }
                }
                else
                {
                    Head = Head.Next;
                    Temp = null;
                }
            }
        }
    }
    // 1 dánh sách liên kết về danh sách các tổ may
    public class clsDanhSachTo
    {
        clsToMay _head, _tail;

        public clsToMay Tail
        {
            get { return _tail; }
            set { _tail = value; }
        }

        public clsToMay Head
        {
            get { return _head; }
            set { _head = value; }
        }

        public clsDanhSachTo()
        {
            Head = null;
            Tail = null;
        }
        public void Sort()
        {
            clsToMay a = new clsToMay(); clsToMay b = new clsToMay();
            for (a = Head; a.Next != null; a = a.Next)
            {
                for (b = a.Next; b.Next != null; b = b.Next)
                {
                    if (a.SanSang > b.SanSang)
                    {

                    }
                }
            }
        }
        public void Add(clsToMay DH)
        {
            clsToMay Temp = new clsToMay();
            Temp.TenTo = DH.TenTo;
            Temp.SanSang = DH.SanSang;
            Temp.Thoigian = DH.Thoigian;
            clsSanPham sp = new clsSanPham();
            sp = DH.NangSuat.Head;
            Temp.NangSuat = new clsDanhSachSanPham();
            while (sp != null)
            {

                Temp.NangSuat.Add(sp);
                sp = sp.Next;
            }
            if (Head == null)
            {
                Head = Tail = Temp;
            }
            else
            {
                if (Temp.SanSang >= Tail.SanSang)
                {
                    Tail.Next = Temp;
                    Tail = Temp;
                }
                else if (Temp.SanSang <= Head.SanSang)
                {
                    Temp.Next = Head;
                    Head = Temp;
                }
                else
                {
                    clsToMay a = new clsToMay();
                    a = Head;
                    while (a != null)
                    {
                        if (Temp.SanSang <= a.SanSang)
                        {
                            clsToMay t = new clsToMay();
                            Temp.Next = a.Next;
                            a.Next = Temp;
                            break;
                        }
                        a = a.Next;
                    }
                }
            }
        }

        public void Delete(clsToMay DH)
        {
            clsToMay p = new clsToMay();
            clsToMay q = new clsToMay();

            p = Head;
            if (p == null)
            {
                return;
            }
            else
            {
                q = p; p = p.Next;

                if (p != null)
                {
                    if (p == Tail)
                    {
                        Tail = q;
                        Tail.Next = null;
                    }
                    else
                    {
                        q.Next = p.Next;
                        p = null;
                    }
                }
                else
                {
                    Head = Head.Next;
                    p = null;
                }
            }
        }
    }


}
