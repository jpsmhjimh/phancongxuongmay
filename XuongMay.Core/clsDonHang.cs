﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XuongMay.Core
{
   public class clsDonHang
    {
        string _maDonHang = "";

        public string MaDonHang
        {
            get { return _maDonHang; }
            set { _maDonHang = value; }
        }
        clsSanPham _sanPham;

        public clsSanPham SanPham
        {
            get { return _sanPham; }
            set { _sanPham = value; }
        }

        string _po="hhh";

        public string Po
        {
            get { return _po; }
            set { _po = value; }
        }
        DateTime _ngayNPL;

        public DateTime NgayNPL
        {
            get { return _ngayNPL; }
            set { _ngayNPL = value; }
        }
        int _soLuongMay = 0;

        public int SoLuongMay
        {
            get { return _soLuongMay; }
            set { _soLuongMay = value; }
        }
        int _soLuongDat;

        public int SoLuongDat
        {
            get { return _soLuongDat; }
            set { _soLuongDat = value; }
        }
        DateTime _ngayBD = new DateTime();

        public DateTime NgayBD
        {
            get { return _ngayBD; }
            set { _ngayBD = value; }
        }
        DateTime _ngayKT = new DateTime();

        public DateTime NgayKT
        {
            get { return _ngayKT; }
            set { _ngayKT = value; }
        }

        public clsDonHang Next = null;
    }
    // Danh sách liên kết về các đơn hàng

    public class clsDanhSachDonHang
    {
        clsDonHang _head = null, _tail = null;

        public clsDonHang Tail
        {
            get { return _tail; }
            set { _tail = value; }
        }

        public clsDonHang Head
        {
            get { return _head; }
            set { _head = value; }
        }
        public void Add(clsDonHang DH)
        {
            clsDonHang temp = new clsDonHang();
            temp.MaDonHang = DH.MaDonHang;
            temp.NgayBD = DH.NgayBD;
            temp.NgayKT=DH.NgayKT;
            temp.NgayNPL = DH.NgayNPL;
            temp.Po = DH.Po;
            temp.SanPham = DH.SanPham;
            temp.SoLuongDat = DH.SoLuongDat;
            temp.SoLuongMay = DH.SoLuongMay;
            

            if (Head == null)
            {
                Head = Tail = temp;
            }
            else
            {
                Tail.Next = temp;
                Tail = temp;
            }
        }

        public void Delete(clsDonHang DH)
        {
            clsDonHang p = new clsDonHang();
            clsDonHang q = new clsDonHang();

            p = Head;
            if (p == null)
            {
                return;
            }
            else
            {
                q = p; p = p.Next;

                if (p == null)
                {
                    if (p != Tail)
                    {
                        Tail = q;
                        Tail.Next = null;
                    }
                    else
                    {
                        q.Next = p.Next;
                        p = null;
                    }
                }
                else
                {
                    Head = Head.Next;
                    p = null;
                }
            }
        }
    }


    // Node đơn hàng nhận được
    public class clsToLam
    {
        clsToMay _chuyenMay;

        public clsToMay ChuyenMay
        {
            get { return _chuyenMay; }
            set { _chuyenMay = value; }
        }
        DateTime _ngayBD;

        public DateTime NgayBD
        {
            get { return _ngayBD; }
            set { _ngayBD = value; }
        }

        DateTime _ngayCat;

        public DateTime NgayCat
        {
            get { return _ngayCat; }
            set { _ngayCat = value; }
        }
        DateTime _ngayVaoChuyen;

        public DateTime NgayVaoChuyen
        {
            get { return _ngayVaoChuyen; }
            set { _ngayVaoChuyen = value; }
        }
        DateTime _ngayRaChuyen;

        public DateTime NgayRaChuyen
        {
            get { return _ngayRaChuyen; }
            set { _ngayRaChuyen = value; }
        }
        int _soNgayDuKien;

        public int SoNgayDuKien
        {
            get { return _soNgayDuKien; }
            set { _soNgayDuKien = value; }
        }
        int _soNgayDuPhong;

        public int SoNgayDuPhong
        {
            get { return _soNgayDuPhong; }
            set { _soNgayDuPhong = value; }
        }

        int _nangSuatDK;

        public int NangSuatDK
        {
            get { return _nangSuatDK; }
            set { _nangSuatDK = value; }
        }

        DateTime _ngayMayXong;
        public DateTime NgayMayXong
        {
            get { return _ngayMayXong; }
            set { _ngayMayXong = value; }
        }

        DateTime _ngayCuoi;

        public DateTime NgayCuoi
        {
            get { return _ngayCuoi; }
            set { _ngayCuoi = value; }
        }



        DateTime _ngayKT;

        public DateTime NgayXuatDi
        {
            get { return _ngayKT; }
            set { _ngayKT = value; }
        }
    }
    public class clsNodeDH
    {
        clsToLam[] _toLam;

        public clsToLam[] ToLam
        {
            get { return _toLam; }
            set { _toLam = value; }
        }
        clsDonHang _donhang;

        public clsDonHang Donhang
        {
            get { return _donhang; }
            set { _donhang = value; }
        }
        clsNodeDH _next;

        public clsNodeDH Next
        {
            get { return _next; }
            set { _next = value; }
        }
    }
    // DS  đơn hàng nhận được
    public class clsDanhSachDonHangNhan
    {
        clsNodeDH _head = null, _tail = null;

        public clsNodeDH Tail
        {
            get { return _tail; }
            set { _tail = value; }
        }

        public clsNodeDH Head
        {
            get { return _head; }
            set { _head = value; }
        }
        public void Add(clsNodeDH DH)
        {
            if (Head == null)
            {
                Head = Tail = DH;
            }
            else
            {
                Tail.Next = DH;
                Tail = DH;
            }
        }

        public void Delete(clsNodeDH DH)
        {
            clsNodeDH p = new clsNodeDH();
            clsNodeDH q = new clsNodeDH();

            p = Head;
            if (p == null)
            {
                return;
            }
            else
            {
                q = p; p = p.Next;

                if (p == null)
                {
                    if (p != Tail)
                    {
                        Tail = q;
                        Tail.Next = null;
                    }
                    else
                    {
                        q.Next = p.Next;
                        p = null;
                    }
                }
                else
                {
                    Head = Head.Next;
                    p = null;
                }
            }
        }
    }

}
